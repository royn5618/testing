'''
Created on 15 Mar 2018

@author: Owner
'''
sports_news = ["https://www.nytimes.com/2018/03/21/sports/jj-redick-sixers.html",
               "https://www.nytimes.com/2018/03/21/sports/michigan-ncaa-jordan-poole.html",
               "https://www.express.co.uk/sport/tennis/935380/Rafael-Nadal-Indian-Wells-runner-up-Daria-Kasatkina-praises-tennis",
               "http://www.skysports.com/football/news/11667/11299352/alexis-sanchez-expected-better-of-himself-since-joining-manchester-united",
               "http://www.bbc.com/sport/cricket/43495372"]

finance_news = ["https://www.cnbc.com/2018/03/20/trade-war-threat-is-now-wall-streets-top-economic-fear-survey-says.html",
                "https://www.cnbc.com/2018/03/21/the-bull-run-isnt-over-for-investors-yet-credit-suisse.html",
                "https://www.wsj.com/articles/citi-says-kushner-cos-loan-was-completely-appropriate-1521644913",
                "https://www.cnbc.com/2018/03/22/china-poised-to-open-markets-beyond-expectations-as-us-tariffs-loom.html",
                "https://www.independent.ie/business/irish/ireland-faces-loss-of-200m-a-year-in-eu-move-36730020.html"]

politcs_news = ["http://www.thejournal.ie/tenants-rent-rises-3916479-Mar2018/",
                 "https://www.irishtimes.com/news/politics/miriam-lord-the-day-32-tds-voted-against-democracy-1.3435663",
                 "https://www.irishtimes.com/news/politics/northern-company-criticised-for-role-in-juvenile-courts-in-egypt-1.3435362",
                 "https://edition.cnn.com/2018/03/21/politics/zuckerberg-cnn-interview-analysis/index.html",
                 "https://edition.cnn.com/2018/03/21/politics/trump-campaign-cambridge-analytica/index.html"]

misc_news = ["https://www.vanityfair.com/hollywood/2018/03/ruth-bader-ginsburg-workout-stephen-colbert"]

categoies = ["economy, business and finance", "politics", "science and technology", "arts, culture and entertainment", "sport", "misc"]