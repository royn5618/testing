'''
Created on 16 Mar 2018

@author: Owner
'''
import textrazor
import LDA_Test.Constants as cc
import json
import requests

#Fetching urls from newsapi
url = ('https://newsapi.org/v2/top-headlines?'
       'country=us&'
       'apiKey=f72b08a088e44d63a28668e1bec3cc59')
response = requests.get(url)
json_data = json.loads(response.text)
all_url = []
for i in range(0,15):
    all_url.append(json_data['articles'][i]['url'])

#processing
default_type = [0, 0 , 0, 0, 0, 0]

textrazor.api_key = "f52dafb1a48b05a19844796f030cc8e66bd36bd15bf521e8b4460ad4"

client = textrazor.TextRazor(extractors=["entities", "topics"])

client.set_cleanup_mode("cleanHTML")
client.set_classifiers(["textrazor_newscodes"])

for url in all_url:
    news_type = [0, 0 , 0, 0, 0, 0]
    print('-------------------')
    print(url)
    print('-------------------')
    response = client.analyze_url(url)
    entities = list(response.entities())
    topics = list(response.topics())
    for category in response.categories():
        if("general" in category.label or "sport" in category.label):
            index = [cc.categoies.index(cat) for cat in cc.categoies if cat in category.label]
            for i in index:
                news_type[i] = 1
    if(news_type == default_type):
        news_type = [0, 0, 0, 0, 0 ,1]
    #print(news_type)
    
    #Extracting entities
    dict_entities = {}
    for entity in entities:
        dict_entities[entity.id] = entity.relevance_score
    top_entities = sorted(dict_entities, key=dict_entities.get, reverse=True)[:10]
    print(top_entities)

    #Extracting entities
    dict_topics = {}
    for topic in topics:
        dict_topics[topic.label] = topic.score
    top_topics = sorted(dict_topics, key=dict_topics.get, reverse=True)[:10]
    print(top_topics)
